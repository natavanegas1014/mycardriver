from django.test import TestCase
from .models import User

# Create your tests here.
def test_create_dn(self):
    response = self.client.post(
        self.uri + 'register/',
        **self.headers,
        data={
            "id": 1,
            "username": "User1",
            "email": "user1@gmail.com",
            "phone": "3101234656"
            "password": "password-2",
            },
        format = 'json'
        )
    self.assertEqual(response.status_code, 200)
