from .models import *
from rest_framework import viewsets
from rest_framework.response import Response
from . import serializers

# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserGetSerializer

    def get_project(self):
        try:
            id = self.kwargs.get('id')
            return id
        except:
            return None
   

    def get_queryset(self):
        id = self.get_project()
        if self.request.user:
            if id is None:
                return self.queryset.all()
            else:
                return self.queryset.filter(id=id, is_delete=False)
        else:
            return self.queryset.none()

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.UserGetSerializer
        elif self.action == 'create':
            return serializers.UserPostSerializer
        else:
            return self.http_method_not_allowed(request=self.request)


    def create(self, request):
        user_data=request.data
        
        if User.objects.filter(username=user_data["username"]).exists():
            return Response({'detail': "user already exists"}, status=400)
      
        User.objects.create(
            username = user_data["username"],
            phone = user_data["phone"],
            rol = user_data["rol"],
            password = user_data["password"],
            address = user_data["address"],
            long = user_data["long"],
		    lat = user_data["lat"],
        )
  
        data = {
            "username": user_data["username"],
            "phone": user_data["phone"],
            "rol": user_data["rol"],
            "password": user_data["password"],
            "address": user_data["address"],
            "long": user_data["lat"], 
            "lat": user_data["lat"]
        }
        
        self.statusSession(user_data["username"], True)

        headers = self.get_success_headers(data)
        return Response(data, status=200, headers=headers)

    def getUserSession(self, request):
        user_input=request.query_params
        print(user_input["username"],dir(user_input))
        if not User.objects.filter(username=user_input["username"], password= user_input["password"]).exists():
            return Response({'detail': "user does not exist"}, status=400)

        user = User.objects.filter(username=user_input["username"]).first()
        self.statusSession(user.id, True)

        return Response({'detail': "login successfully"}, status=400)

    def statusSession(self, username, status):
        user = User.objects.filter(username=username).first()
        session= Session.objects.filter(user_id=user.id)
        if session.exists():
            session.update(status = status)
        else: 
            session.create(user_id=user.id, status = status)

    def closeSession(self, request, id):
        user = User.objects.filter(id=id).first()
        self.statusSession(user.username, False)
        return Response({'detail': "login closed"}, status=200)

class servicesViewSet(viewsets.ModelViewSet):
    def userService(self, request):
        services_user=request.data
        servicesUser.objects.create(
            user_id = services_user["user_id"],
            address = services_user["address"],
            long = services_user["long"], 
            lat = services_user["lat"],
            status_service = services_user["status_service"],
            driver = services_user["driver"],
        )
        return Response({'detail': "send service"}, status=200)

    def getUserServicePending(self, request):
        data_users = User.objects.filter(rol="user")
        servicesPending=servicesUser.objects.filter(user_id= data_users["id"], status_service= 1,)
        print(dir(servicesPending))
        servicesPending()
        return Response(servicesPending, status=200)
        
    def servicesStatus(self, request):
        status=request.data
        servicesStatusUser.objects.create(status=status['status'])
        return Response({'detail': "service ok"}, status=200)

    def servicesDriver(self, request):
        services_drive=request.data
        servicesUser.objects.create(
            driver_id = services_drive["driver_id"],
            address = services_drive["address"],
            long = services_drive["long"], 
            lat = services_drive["lat"],
            status = services_drive["status"],
            condition = services_drive["condition"]
        )
        return Response({'detail': "send service"}, status=200)