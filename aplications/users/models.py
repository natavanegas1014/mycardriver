from django.db import models

# Create your models here.

class User(models.Model):
    id = models.AutoField(primary_key = True)
    username = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)
    rol = models.CharField(max_length=20)
    password = models.CharField(max_length=10)
    address = models.CharField(max_length=100)
    long = models.CharField(max_length=20)
    lat = models.CharField(max_length=20)
    class Meta:
        db_table = 'user'
        ordering = ['-id']

    def __str__(self):
        return self.id

class Session(models.Model):
    id = models.AutoField(primary_key = True)
    user_id = models.IntegerField(default=0)
    status = models.BooleanField(default=False)

    class Meta:
        db_table = 'session'
        ordering = ['-id']

    def __str__(self):
        return self.id

class servicesUser(models.Model):
    id = models.AutoField(primary_key = True)
    user_id = models.IntegerField(default=0)
    address = models.CharField( max_length=100)
    long = models.CharField(max_length=20)
    lat = models.CharField(max_length=20)
    status_service = models.IntegerField(default=0)
    driver = models.CharField(null=True, max_length=20)
    
    class Meta:
        db_table = 'services_user'
        ordering = ['-id']

    def __str__(self):
        return self.id

class servicesStatusUser(models.Model):
    id = models.AutoField(primary_key = True)
    status = models.CharField(max_length=100)
    class Meta:
        db_table = 'services'
        ordering = ['-id']

    def __str__(self):
        return self.id

class servicesDriver(models.Model):
    id = models.AutoField(primary_key = True)
    driver_id = models.IntegerField(default=0)
    address = models.CharField(max_length=100)
    long = models.CharField(max_length=20)
    lat = models.CharField(max_length=20)
    status = models.BooleanField(default=True)
    condition = models.CharField(max_length=20)

    class Meta:
        db_table = 'services_driver'
        ordering = ['-id']

    def __str__(self):
        return self.id