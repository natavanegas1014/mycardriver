"""singosgu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path,re_path
from django.conf import settings
from django.conf.urls.static import static
from . import views


urlpatterns = [
    path(r'', views.UserViewSet.as_view({"get": "list"}), name="users"),
    path(r'register', views.UserViewSet.as_view({"post":"create"}), name='create'),
    path(r'login', views.UserViewSet.as_view({"get":"getUserSession"}), name='login'),
    re_path(r'^logout/(?P<id>\d+)/$', views.UserViewSet.as_view({"get":"closeSession"}), name='logout'),
    path(r'service/user', views.servicesViewSet.as_view({"post":"userService"}), name='userService'),
    path(r'service/driver', views.servicesViewSet.as_view({"post":"servicesDriver"}), name='driverService'),
    path(r'service/pending', views.servicesViewSet.as_view({"get":"getUserServicePending"}), name='userpending'),
    path(r'service/status', views.servicesViewSet.as_view({"post":"servicesStatusUser"}), name='userService'),
    path(r'service', views.servicesViewSet.as_view({"put":"requestService"}), name='userService'),
]
