from rest_framework import serializers
from .models import *
from rest_framework.response import Response


class UserGetSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True)
    phone = serializers.CharField(read_only=True)
    rol = serializers.CharField(read_only=True)
    password = serializers.CharField(read_only=True)
    address = serializers.CharField(read_only=True)
    long = serializers.CharField(read_only=True)
    lat = serializers.CharField(read_only=True)
    class Meta:
        model = User
        fields = '__all__'
        read_only_fields = ['id', ]

class UserPostSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=False)
    phone = serializers.CharField(read_only=False)
    rol = serializers.CharField(read_only=False)
    password = serializers.CharField(read_only=False)
    address = serializers.CharField(read_only=False)
    long = serializers.CharField(read_only=False)
    lat = serializers.CharField(read_only=False)
    class Meta:
        model = User
        fields = '__all__'
        read_only_fields = ['id', ]